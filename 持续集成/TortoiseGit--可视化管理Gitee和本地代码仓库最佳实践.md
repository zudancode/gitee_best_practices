一、TortoiseGit简介
--
> `TortoiseGit`是`Tortoise`基于`git`的可视化管理工具。本文即将介绍这个工具的安装和简单使用教程（本文均是基于Windows 64位操作系统）。

>git的管理工具有很多。Tortoise有以下优点。

1. 可视化管理，非命令行操作

2. 支持简体中文

3. 和资源管理器紧密结合

4. 操作简单

5. 日常维护码云上的协作代码很方便

 二、Git安装
--
 

> 参考[http://www.runoob.com/git/git-install-setup.html](http://www.runoob.com/git/git-install-setup.html)

三、TortoiseGit安装
--

>下载TortoiseGit，他的程序和语言包是分开的，两个都下载，在安装程序的时候会让你选择语言，此时安装语言包，刷新，下拉选择简体中文。

>下载地址：[https://tortoisegit.org/download/](https://tortoisegit.org/download/)

![](https://img-blog.csdnimg.cn/20190528161325626.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

>两个都安装完成后`重启`电脑。然后鼠标右击会多出如下几个选项。

![](https://img-blog.csdnimg.cn/20190528161910167.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

四、使用码云创建项目
--
使用码云（https://gitee.com）优点如下：

1. 免费

2. 私有库（免费）

3. 中文

4. 简单

5. 国产（详见：[开源界也要注意，Apache 基金会与 GitHub 都受美国法律约束](https://www.oschina.net/news/106836/opensource-ourself)）

>注册号码云帐号后根据需求新建仓库，界面如下：

![](https://img-blog.csdnimg.cn/20190528164241460.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

>创建完成后复制链接地址,后面步骤备用，如下图所示：

![](https://img-blog.csdnimg.cn/20190528164732925.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

五、TortoiseGit简单使用
--

>介绍基础的使用方法

1. 创建项目的文件夹，右击该文件夹，选择克隆（自动创建本地库）。

![](https://img-blog.csdnimg.cn/20190529110142118.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

2. 把刚刚复制的git项目的链接粘贴到URL里，并核对本地库的目录，然后点击确定。

 ![](https://img-blog.csdnimg.cn/20190529110253204.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

完成后会有如下变化：

>文件夹上会多一个绿色勾标记。

![](https://img-blog.csdnimg.cn/20190529110401990.png)

>创建项目的时候自带的文件已经同步到本地

![](https://img-blog.csdnimg.cn/20190529110557745.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

3. 上传文件，分为两个操作，`提交`和`推送`，

`提交`是将文件添加到本地版本控制里面，并没有提交到远程项目里.

`推送`是提交到远程项目里。

>我们新建一个文件new1.txt，然后右击文件夹同步或者直接右击文件提交。下面是同步界面，常用的有提交、推送和拉取。

![](https://img-blog.csdnimg.cn/20190529110822816.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

>`提交`，点击提交进入下面的界面，我们可以勾选需要提交的文件，同时填写备注（必填）

![](https://img-blog.csdnimg.cn/20190529111209665.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

>点击`提交`后会弹出让你推送的界面，推送是传至远程项目(也就是`码云`端)。

![](https://img-blog.csdnimg.cn/20190529111337987.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

>完成后我们可以打开码云上对应项目查看，项目里会多一个我们上传的文件。

![](https://img-blog.csdnimg.cn/20190529111446937.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

4. `拉取`

>我们直接在码云项目上传一个文件new2.txt（网页上操作），模拟另一个成员推送新文件.

![](https://img-blog.csdnimg.cn/20190529112106993.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

>然后我们右击本地库文件夹，选择同步，然后点击拉取，我们就能从远程项目上拉取到新的文件

![](https://img-blog.csdnimg.cn/20190529111554368.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwOTU0OTU5,size_16,color_FFFFFF,t_70)

>本地文件夹上就会多一个文件

![](https://img-blog.csdnimg.cn/20190529112233432.png)

>整个一个完整的过程就是这样了，大家可以自己探索更多操作，解锁更多姿势。



